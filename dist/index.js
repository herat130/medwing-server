'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _connectTimeout = require('connect-timeout');

var _connectTimeout2 = _interopRequireDefault(_connectTimeout);

var _dotenv = require('dotenv');

var _dotenv2 = _interopRequireDefault(_dotenv);

var _compression = require('compression');

var _compression2 = _interopRequireDefault(_compression);

var _helmet = require('helmet');

var _helmet2 = _interopRequireDefault(_helmet);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_dotenv2.default.config();

var app = (0, _express2.default)();
var router = _express2.default.Router();

app.use((0, _cors2.default)());
app.use((0, _compression2.default)());
app.use((0, _helmet2.default)());
app.use((0, _connectTimeout2.default)('100s'));
app.use(haltOnTimedout);

function haltOnTimedout(req, res, next) {
    if (!req.timedout) next();
}

app.get('/admin', function (req, resp) {
    try {
        resp.send([{ name: '' + process.env.API_KEY }]);
    } catch (err) {
        resp.status(500).send();
    }
});

app.listen(80, function () {
    console.log("listning at port 80");
});